#!/bin/sh

cd ../jekyll && \
docker run --rm \
        --volume="$PWD:/srv/jekyll" \
        -p 8000:8080 \
        -p 35729:35729 \
        -it registry.gitlab.com/mr-anderson/mr-anderson/jekyll:latest \
        jekyll serve --watch --livereload --host 0.0.0.0 --port 8080
