#!/bin/sh

IMAGE_TAG="registry.gitlab.com/mr-anderson/mr-anderson/jekyll:latest"

cd .. && \
docker build --file Dockerfile.jekyll --tag ${IMAGE_TAG} .

echo ""
echo ""
echo "If the created image works, do not forget to run this:"
echo "docker push ${IMAGE_TAG}"
echo ""
