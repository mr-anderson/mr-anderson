#!/bin/sh

cd ../jekyll && \
docker run --rm \
        --volume="$PWD:/srv/jekyll" \
        -t registry.gitlab.com/mr-anderson/mr-anderson/jekyll:latest \
        jekyll build . && \
cd .. && \
docker build -t mr-anderson . && \
docker stop mr-anderson-container;
docker rm mr-anderson-container;
docker run --name mr-anderson-container -d -p 80:80 mr-anderson

