---
layout: page
title: Variants
permalink: /variants/
---

{% for variant in site.variants %}
  <h2>
    <a style="color:#{{ variant.hex-color | default: "000000" }}" >&#9724; </a>
    <a href="{{ variant.url }}"> {{ variant.name }} </a>
  </h2>
  <p>{{ variant.summary }}</p>
{% endfor %}
