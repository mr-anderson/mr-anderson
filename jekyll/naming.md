---
name: Naming
summary: Why did I choose this name
layout: page
---
# Naming

I wanted a better name than _my-load-testing-experiments_ and seeing that the project focused dealing with being 
"shot at with a lot of bullets" (Gatling) I thought about [Ikaruga](https://youtu.be/kIxKjmrCrS4?t=312) but ended up 
going with a [Matrix](https://en.wikipedia.org/wiki/The_Matrix) reference. This was mostly because of the 
_"...I can dodge bullets? ...you won't have to"_ exchange, indicating the best approaches won't have to drop requests 
(or use suboptimal solutions such as unmaintainable optimisations) but will have more innovative ways to deal with them.

"Neo" is too short/indistinct/obvious and implies I have already solved the problem, so I ended up picking
[mr-anderson](https://gitlab.com/mr-anderson) .

The Gatling scripts are called [agents](https://gitlab.com/mr-anderson/agents) (in theme with the above), the base API 
is simply called [api](https://gitlab.com/mr-anderson/mr-anderson_api). Variants are named with a randomly selected color
from the list below to keep things simple, equal, distinct and low-effort. 

{% comment %}
https://www.randomlists.com/random-color
https://en.wikipedia.org/wiki/X11_color_names
{% endcomment %}

1. lemon-chiffon
1. indigo
1. wheat
1. sienna
1. pale-green
1. slate-blue
1. blue-violet
1. spring-green
1. honeydew
1. royal-blue
1. pale-violet-red
1. fire-brick
1. alice-blue
1. steel-blue
1. navajo-white
1. green-yellow
1. tomato
1. maroon
1. mint-cream
1. goldenrod
1. moccasin
1. dim-gray
1. saddle-brown
1. chartreuse
1. burly-wood
1. papaya-whip
1. sea-green
1. old-lace
1. cadet-blue
1. antique-white
1. cornsilk
1. khaki
1. thistle
1. peach-puff
1. olive-drab
1. crimson
1. chocolate
1. slate-gray
1. misty-rose
1. lime-green
1. blanched-almond
1. ghost-white
1. midnight-blue
1. indian-red
1. pale-turquoise
1. deep-sky-blue
1. pale-goldenrod
1. floral-white
1. lavender
1. olive-green
