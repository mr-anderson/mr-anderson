---
layout: variant
name: API
summary: (REST) API that all other variants call for their data
hex-color: F4F4F4
---
Not really a variant, but the canonical source of truth about the state of the data that all variants should represent.

Server by [JSON Server](https://github.com/typicode/json-server), who's state (on startup) is configured
[here](https://gitlab.com/mr-anderson/mr-anderson_api/blob/master/dataset-seed.js).

JSON Server is configured to add a delay (100ms at the moment of writing) before responding to each request. This is done
to simulate a slow data store that cannot be sped up directly by the variants. The delay also allows the variants something
to improve upon, as JSON Server is pretty fast already.

For comparison the Gatling load tests are also run against the API.
