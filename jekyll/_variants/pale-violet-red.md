---
layout: variant
name: pale-violet-red
summary: Frontend cached version of spring-green
hex-color: DB7093
---

Basically [burly-wood](/variants/burly-wood), but with the cache in front of the spring-green container instead of on its
back. Ie: in this variant the requests from the browser to the spring-green container are cached, instead of the ones from
spring-green to the api.

### Lessons learned
- Building on my experience with overriding docker-compose during development in burly-wood, I made a development specific
    Nginx (cache) configuration. This required some fiddling with Nginx includes, but allowed much easier testing etc.
- [docker-compose-viz](https://github.com/pmsipilot/docker-compose-viz), which allows easy visualisation of
    a docker-compose configuration via a diagram (see below). I'll create one for each variant.
