---
layout: variant
name: spring-green
summary: First baseline implementation
hex-color: 00FF7F
---

Basically a pass-through PHP implementation of [the api](/variants/api), intended as a baseline to compare better
solutions to. It is however not a simple proxy: each request is handled like in a regular PHP web app: routed to a controller,
interpreted, data is retrieved via a repository and the result is formatted and returned. 
