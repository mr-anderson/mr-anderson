---
layout: variant
name: burly-wood
summary: Backend cached version of spring-green
hex-color: DEB887
---
This variant uses the docker image from [spring-green](/variants/spring-green), but caches all the requests that that 
container makes to the API with Nginx. So the request from the end-user always goes to the spring-green container, but
requests from the spring-green container to the API might get cached by Nginx.

Initially the Nginx cache will be configured to cache everything it can. In the future this configuration will require
re-consideration, as the requirements of the Gatling script start including changes to the state of the data,
making the cache invalid.

### Building
Building this variant was significantly more work than I expected. This was partially due to the fact that I (initially)
wanted the spring-green image to remain unchanged, which required me to redirect its calls to the API from outside the
image. After many new lessons in (docker) networking, [HOSTALIASES](http://blog.tremily.us/posts/HOSTALIASES/), etc. I
found no solution that could be implemented in reasonable time. In the end I changed the spring-green image so it would 
read the API url from an environment variable.

Configuring Nginx to cache JSON server had its own challenges: JSON server returns some headers indicating its output
should not be cached. Complementary to that I missed something in the "Can Cache-Control Headers Be Ignored?" section of
the [Nginx caching guide](https://www.nginx.com/blog/nginx-caching-guide/), namely that ignoring the Cache-Control headers
requires setting a value for ```proxy_cache_valid```. If you don't do that, Nginx will quietly not cache anything.

This was also the first project where I added a docker-compose development override config file. This allowed me to
approach the cache container directly with my browser, which made figuring out all the above a LOT easier. Usage:

- make a docker-compose.yml production config
- make a docker-compose.development.yml file that overrides/supplements certain parts to make development easier
- run your local containers like this:
 - ```docker-compose -f docker-compose.yml -f docker-compose.development.yml up```


### Lessons learned
- a lot about DNS (overriding methods) and (docker) networking
- a lot about caching, both from the browser and the Nginx configuration side
- development specific config override for docker-compose

